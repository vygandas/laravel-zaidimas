<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Kovotojo savybės ir metodai
     */

    private $health;
    private $baseDamage;
    private $baseDefence;
    private $criticalIncrease;
    private $criticalRate;
    private $items;

    function __construct($name = '')
    {
        parent::__construct();

        $this->health = 100;
        $this->baseDamage = 3;
        $this->baseDefence = 1;
        $this->criticalIncrease = 3; // +10%
        $this->criticalRate = 0.1; // 10%
//        if ($this->id == 1) dd($this->items);

    }

    public function loadItems() {
        $this->items = Item::leftJoin('user_items', 'user_items.item_id', '=', 'items.id')
            ->where('user_items.user_id', $this->id)
            ->get();

    }

    public function getHealth()
    {
//        dd($this->id);
        return $this->health;
    }

    public function getBaseDamage() {
        $dmgFromItems = $this->items->sum('damage');
        return $this->baseDamage + $dmgFromItems;
    }

    public function getDamage()
    {
        $chance = rand(0, 10) / 10; // 0 .. 1
        if ($chance <= $this->getCriticalRate()) {
            return ($this->baseDamage + $this->getBaseDamage()) * $this->criticalIncrease;
        }
        return $this->baseDamage + $this->getBaseDamage();
    }

    public function getDefence()
    {
        $defFromItems = $this->items->sum('defence');
        return $this->baseDefence + ($defFromItems ?? 0);
    }

    public function getCriticalRate()
    {
        $criticalRateFromItems = $this->items->sum('critical');
        return $this->criticalRate + ($criticalRateFromItems ?? 0);
    }

    public function isAlive(): bool
    {
        return $this->health > 0;
    }

    public function receiveDamage(int $damage)
    {
        $actualDamage = $damage - $this->getDefence();
        $this->health = $this->health - $actualDamage;
    }











}
