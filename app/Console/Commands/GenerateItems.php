<?php

namespace App\Console\Commands;

use App\Item;
use App\Types\ItemType;
use Illuminate\Console\Command;

class GenerateItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Item::create([
            'name' => 'Puodas',
            'type' => ItemType::Offence,
            'damage' => 5,
            'critical' => 8,
            'defence' => 0
        ]);

        Item::create([
            'name' => 'Kočėlas',
            'type' => ItemType::Offence,
            'damage' => 9,
            'critical' => 17,
            'defence' => 0
        ]);

        Item::create([
            'name' => 'Skarelė',
            'type' => ItemType::Defence,
            'damage' => 0,
            'critical' => 0,
            'defence' => 4
        ]);

        Item::create([
            'name' => 'Vasarinė striukė',
            'type' => ItemType::Defence,
            'damage' => 0,
            'critical' => 0,
            'defence' => 9
        ]);

        Item::create([
            'name' => 'Kailiniai',
            'type' => ItemType::Defence,
            'damage' => 0,
            'critical' => 0,
            'defence' => 45
        ]);

    }
}
