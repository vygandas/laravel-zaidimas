<?php
/**
 * Created by PhpStorm.
 * User: vygandas
 * Date: 22/06/2018
 * Time: 10:06
 */

namespace App;


class Player extends User
{
    private $health;
    private $baseDamage;
    private $baseDefence;
    private $criticalIncrease;
    private $criticalRate;
    private $name;

    function __construct($name = '')
    {
        parent::__construct();

        $this->health = 100;
        $this->baseDamage = 3;
        $this->baseDefence = 1;
        $this->criticalIncrease = 3; // +10%
        $this->criticalRate = 0.1; // 10%
        $this->name = $name;
    }

    function __toString()
    {
        return $this->name;
    }

    function getHealth()
    {
        return $this->health;
    }

    function getDamage()
    {
        $chance = rand(0, 100) / 100; // 0 .. 1
        if ($chance <= $this->getCriticalRate()) {
            return $this->baseDamage * $this->criticalIncrease;
        }
        return $this->baseDamage;
    }

    function getDefence()
    {
        return $this->baseDefence;
    }

    function getCriticalRate()
    {
        return $this->criticalRate;
    }

    function isAlive(): bool
    {
        return $this->health > 0;
        /*
         if ($this->health > 0) {
            return true;
        } else {
            return false;
        }
         */
    }

    function receiveDamage(int $damage)
    {
        $actualDamage = $damage - $this->getDefence();
        $this->health = $this->health - $actualDamage;
    }
}