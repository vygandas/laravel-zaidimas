<?php
namespace App\Types;

abstract class ItemType {
    const Offence = 'offence';
    const Defence = 'defence';
}