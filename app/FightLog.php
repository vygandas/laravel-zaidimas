<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FightLog extends Model
{
    protected $fillable = [
        'fight_id',
        'user_1_id',
        'user_2_id',
        'user_1_health',
        'user_2_health',
        'user_1_damage',
        'user_2_damage',
        'is_critical',
    ];
}
