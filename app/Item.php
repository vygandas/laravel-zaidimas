<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $fillable = [
        'name',
        'type',
        'damage',
        'critical',
        'defence'
    ];
}
