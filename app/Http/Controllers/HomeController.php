<?php

namespace App\Http\Controllers;

use App\Engine\Arena;
use App\FightLog;
use App\Item;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        SELECT * FROM users WHERE id <> ?
        $users = User::where('id', '<>', auth()->id())
            ->get();
        return view('home')
            ->with('users', $users);
    }

    public function arena($id) {
        if ($id == auth()->id()) {
            return redirect('home')
                ->with('status', 'Su savim pyktis negerai.');
        }
//        SELECT * FROM users WHERE id = ? LIMIT 1;
        $user1 = User::where('id', $id)->first();

        $arena = new Arena();
        auth()->user()->loadItems();
        $user1->loadItems();
        $arena->setPlayer1( auth()->user() );
        $arena->setPlayer2( $user1 );
        $fightResult = $arena->fight();

//        SELECT * FROM items I
//        LEFT JOIN user_items UI on UI.item_id = I.id
//        WHERE UI.user_id = 1
        $myItems = Item::leftJoin('user_items', 'user_items.item_id', '=', 'items.id')
            ->where('user_items.user_id', auth()->id())
            ->get();
        $user1Items = Item::leftJoin('user_items', 'user_items.item_id', '=', 'items.id')
            ->where('user_items.user_id', $user1->id)
            ->get();

        $fightLogs = FightLog::where('fight_id', $fightResult->id)->get();

        return view('arena')
            ->with('user1', $user1)
            ->with('fightResult', $fightResult)
            ->with('myItems', $myItems)
            ->with('user1Items', $user1Items)
            ->with('fightLogs', $fightLogs);
    }
}
