<?php
namespace App\Engine;

use App\Fight;
use App\FightLog;
use App\Player;

class Arena
{
    /**
     * @var Player
     */
    private $player1;
    /**
     * @var Player
     */
    private $player2;
    /**
     * @var Player
     */
    private $winnerPlayer;

    public function setPlayer1($player1)
    {
        $this->player1 = $player1;
//        $this->printInfo("Priskirtas 1 žaidėjas");
    }

    public function setPlayer2($player2)
    {
        $this->player2 = $player2;
//        $this->printInfo("Priskirtas 2 žaidėjas");
    }

    private function printInfo($text) {
        echo "<tr><td>$text</td></tr>";
    }

    private function fightLog($fightId, $data) {
        FightLog::create(array_merge([
            'fight_id' => $fightId,
            'user_1_id' => $this->player1->id,
            'user_2_id' => $this->player2->id,
            'user_1_health' => $this->player1->getHealth(),
            'user_2_health' => $this->player2->getHealth(),
        ], $data));
    }

    public function fight() {
        $fight = new Fight();
        $fight->user_1_id = $this->player1->id;
        $fight->user_2_id = $this->player2->id;
        $fight->user_winner_id = -1;
        $fight->save();

        $bothAlive = true;
        while ( $bothAlive ) {
            $player1Damage = $this->player1->getDamage();
//            dd($player1Damage);
            $this->player2->receiveDamage( $player1Damage );
//            $this->printInfo(
//                "1 žaid. puolimas: $player1Damage"
//                . " | 2 žaid. liko gyv. " . $this->player2->getHealth()
//            );
            $this->fightLog($fight->id, [
                'user_1_damage' => $player1Damage,
                'is_critical' => $this->player1->getBaseDamage() < $player1Damage
            ]);
            if (!$this->player2->isAlive()) {
                $this->winnerPlayer = $this->player1;
                break;
            }

            $player2Damage = $this->player2->getDamage();
            $this->player1->receiveDamage( $player2Damage );
//            $this->printInfo(
//                "2 žaid. puolimas: $player2Damage"
//                . " | 1 žaid. liko gyv. " . $this->player1->getHealth()
//            );
            $this->fightLog($fight->id, [
                'user_2_damage' => $player2Damage,
                'is_critical' => $this->player2->getBaseDamage() < $player2Damage
            ]);
            if (!$this->player1->isAlive()) {
                $this->winnerPlayer = $this->player2;
                break;
            }

            $bothAlive = $this->player1->isAlive() && $this->player2->isAlive();

        }
//echo "<b>".$fight->id."</b>";
        $fight->user_winner_id = $this->winnerPlayer->id;
        $fight->winner_health = $this->winnerPlayer->getHealth();
        $fight->save();

        $this->updateUserStats(); // !!!

        return $fight;
    }

    private function updateUserStats() {
        if($this->winnerPlayer->id == $this->player1->id) {
            $this->player1->win_count++;
            $this->player1->save();
            $this->player2->loose_count++;
            $this->player2->save();
        } else {
            $this->player2->win_count++;
            $this->player2->save();
            $this->player1->loose_count++;
            $this->player1->save();
        }
    }

}