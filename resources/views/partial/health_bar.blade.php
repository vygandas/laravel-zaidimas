<div class="progress" style="margin: 25px 0;">
    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70"
         aria-valuemin="0" aria-valuemax="100" style="width:{{$hp}}%;     background-color: red;">
        <span class="sr-only">Liko {{$hp}}% gyvybių</span>
    </div>
</div>