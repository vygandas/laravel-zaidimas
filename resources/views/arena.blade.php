@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        {{ auth()->user()->name }}
                        <span style="color: red;">VS</span>
                        {{ $user1->name }}
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif



                        <table class="table table-bordered">
                            <thead>
                                <th>{{ auth()->user()->name }}</th>
                                <th>{{ $user1->name }}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="mb-2">
                                            <div> DMG:
                                                {{ auth()->user()->getBaseDamage() }}</div>
                                            <div>CRIT:
                                                {{ auth()->user()->getCriticalRate() }}%</div>
                                            <div>DEF:
                                                {{ auth()->user()->getDefence() }}</div>
                                        </div>

                                        @foreach($myItems as $item)
                                            <div class="badge badge-info mb-1">
                                                {{ $item->name }}
                                                (
                                                +{{ $item->damage }} dmg. /
                                                +{{ $item->critical }} % /
                                                +{{ $item->defence }} def.
                                                )
                                            </div>
                                        @endforeach
                                    </td>
                                    <td>
                                        <div class="mb-2">
                                            <div>DMG:
                                                {{ $user1->getBaseDamage() }}</div>
                                            <div>CRIT:
                                                {{ $user1->getCriticalRate() }}%</div>
                                            <div>DEF:
                                                {{ $user1->getDefence() }}</div>
                                        </div>

                                        @foreach($user1Items as $item)
                                            <div class="badge badge-info mb-1">
                                                {{ $item->name }}
                                                (
                                                +{{ $item->damage }} dmg. /
                                                +{{ $item->critical }} % /
                                                +{{ $item->defence }} def.
                                                )
                                            </div>
                                        @endforeach
                                    </td>
                                </tr>
                            </tbody>
                        </table>



                        @if($fightResult->user_winner_id == auth()->id())
                            <div class="alert alert-success">
                                Laimėjau aš :)
                            </div>
                        @else
                            <div class="alert alert-danger">
                                Laimėjo {{ $user1->name }} :(
                            </div>
                        @endif




                        <table class="table" id="healthBars">
                            <thead>
                            <th style="width: 50%;">{{ auth()->user()->name }}</th>
                            <th style="width: 50%;">{{ $user1->name }}</th>
                            </thead>
                            <tbody>
                            @foreach($fightLogs as $log)
                                <tr class="hidden">
                                    <td>
                                        @include('partial.health_bar', [ 'hp' => $log->user_1_health ])
                                    </td>
                                    <td>
                                        @include('partial.health_bar', [ 'hp' => $log->user_2_health ])
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <style>
                            #healthBars .hidden { display: none; }
                        </style>
                        <script>
                            var started = false;
                            var timer = setInterval(function () {
                                if (started) {
//                                    $('#healthBars tbody>.hidden:first').remove();
                                    $('#healthBars .rmthis').empty();
                                }
                                $('#healthBars tbody>.hidden:first').removeClass('hidden').addClass('rmthis');

                                started = true;
                            }, 600);
                        </script>




                        <div>
                            <?php
                            $hp = $fightResult->winner_health / 100 * 100;
                            ?>
                            Laimėtojui liko
                            @include('partial.health_bar', [ 'hp' => $hp ])
                        </div>
                        <div>
                            <img src="/giphy.gif"/>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection