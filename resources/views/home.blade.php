@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Vartotojai</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-danger">
                            {{ session('status') }}
                        </div>
                    @endif



                    @if(!empty($users))
                        @foreach($users as $user)
                            <div>
                                <span>
                                    {{ $user->name }}
                                    (
                                    {{ $user->win_count }}
                                    /
                                    {{ $user->loose_count + $user->win_count }}
                                    )
                                </span>
                                <a href="{{ route('arena', $user->id) }}"
                                   class="btn btn-danger"
                                >
                                    Kovoti!
                                </a>
                            </div>
                        @endforeach
                    @endif



                </div>
            </div>
        </div>
    </div>
</div>
@endsection
