# Laravel PHP OOP pamoka

## Projekto paleidimas
 
Pirmiausia reikia sukurti .env failą su visais reikiamais nustatymais.
Nustatymų teksto pavyzdys laikomas `env.example` faile.

Duomenų bazės nustatymai atrodo taip:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```
Reikia pakoreguoti atitinkamai pagal savo poreikius.

Laravel serverio startavimas naudojant komandinę eilutę (terminalą):
```
php artisan serve
```

(Jei 80 portas užimtas, galima pridėti parametrą `--port=8080`)

Jeigu meta klaidą, kad negalima įrašyti į failus (<...> permission denied <...>)
ir jei dirbame IOS / Linux aplinkoje, labai tikėtina, kad
serveris neturi teisės įrašinėti. Šios komandos padės:
```
sudo chmod -R 777 storage
sudo chmod -R 777 bootstrap/cache
```

## Stiliai ir JS

Vieną kartą, tik pradedant dirbti su projektu reikia paleisti komandą
```
npm install
```
Taip bus parsiųstos visos bibliotekos, reikalingos CSS ir JS kompiliavimui.



Norint, kad sukompiliuotu mūsų aprašytus stilius ir JavaScript kodą, reikia paleisti šią komandą:

```
npm run dev
```

Jei norime dirbti ir kad automatiškai kompiliuotų mūsų css ir js:

```
npm run watch
```

Jei norime paruošti css ir js produkcijai (variantą, kuris pateikiamas lankytojams):
```
npm run prod
```