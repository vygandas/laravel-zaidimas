<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFightLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fight_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fight_id');
            $table->integer('user_1_id');
            $table->integer('user_2_id');
            $table->decimal('user_1_health')->nullable()->default(null);
            $table->decimal('user_2_health')->nullable()->default(null);
            $table->decimal('user_1_damage')->nullable()->default(null);
            $table->decimal('user_2_damage')->nullable()->default(null);
            $table->boolean('is_critical')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fight_logs');
    }
}
